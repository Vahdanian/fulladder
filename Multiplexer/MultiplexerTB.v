`include "Multiplexer.v"
`timescale 1ns / 1ns
`include "../ClkGenerator/ClkGenerator.v"


module MultiplexerTB;
    wire clk;
    reg[ 31:0] X, Y;
    wire [31:0] out;
    reg select

    clock_generator clock (clk);
    Multiplexer mux (X, Y, select , out);

    initial begin
      $dumpvars(0, MultiplexerTB);

      @(posedge clk); X = 0; Y = 32'h12341234; select <= 0; display; @(posedge clk); selection <= 1; display;

      @(posedge clk); X = 0; Y = 32'h98762341; select <= 0; display; @(posedge clk); selection <= 1; display;

      @(posedge clk);  X = 32'h112233; Y=0; select <= 0; display; @(posedge clk); selection <= 1; display;

      #20 $finish;
    end

    task display;
      #1 $display("X = %0h Y = %0h select = %0b out = %0h", X, Y, select, out);
    endtask
endmodule 
