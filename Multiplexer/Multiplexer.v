module multiplexer (X , Y , select, out);
    parameter n = 32;
    input [n-1:0] X, Y;
    input select;
    output [n-1:0] out;

    assign out = (select == 1) ? Y : X;
endmodule
