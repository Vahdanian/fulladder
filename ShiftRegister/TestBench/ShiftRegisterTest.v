module Tests;

    reg Clock,Pload,RightToLeft,SLData , SRData;
    reg [2:0] inputReg;
    wire [2:0] out;

    always
        begin
            Clock = 0;
            #3;
            Clock = 1;
            #3;
        end

    ShiftRegister shiftRegister(.PLoad(Pload), .Input(inputReg), .SLData(SLData), .SRData(SRData), .Clock(Clock), .Q(out));

    initial begin
        Pload = 0;
        RightToLeft = 0;
        SLData= 1;
        SRData = 0;
        $display("case 1");
        #12 $display("%b", out);
        #12 $display("%b", out);
        #12 $display("%b", out);
        RightToLeft = 1;
        $display("case 2");
        #12 $display("%b", out);
        #12 $display("%b", out);
        inputReg = 3'b100;
        Pload = 1;
        $display("Pload to 100");
        #12 $display("%b", out);
        inputReg = 3'b110;
        $display("Pload to 110");
        #12 $display("%b", out);
        #12 $display("%b", out);
    end

endmodule
