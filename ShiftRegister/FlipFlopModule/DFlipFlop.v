module DFlipFlop (D,Clock,Q,QNot);
    input wire D,Clock;
    output reg Q,Qnot;
    always @(posedge Clock)
    begin
        Q=D;
        Qnot=~D;
    end
endmodule : DFlipFlop
