module ShiftRegister(PLoad, Input, RightToLeft , SLData, SRData, Clock, Q);
    parameter n = 3;
    input Enable, Reset, PLoad, RightToLeft, SLData, SRData, Clock;
    input [n-1:0] Input;
    reg [n-1:0] InputReg;
    output wire [n-1:0] Q;
    output wire [n-1:0] nQ;


    generate for(genvar i=0; i < n; i=i+1)
            DFlipFlop dff(.D(InputReg[i]), .Clock(Clock), .Q(Q[i]),.nQ(nQ[i]));
    endgenerate

    always @(posedge Clock)
        begin
            if (PLoad)
                InputReg = Input;
            else
                begin
                    if (RightToLeft)
                        InputReg = {SLData, Q[n-1:1]};
                    else
                        InputReg = {Q[n-2:0], SRData};
                end
        end
endmodule
